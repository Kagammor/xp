<?php include('header.php'); ?>
		<div id="nav">
			<a href="./">&#8592; Index</a> <span style="color: #aaa;">|<span> 
			<a href="./batch">&#8592; batches</a>
		</div>
		
		<h2>Batches</h2>
		
		<?php
			// Check if user has any batches
			$query_batch = "SELECT * FROM batches WHERE user='" . checkUser() . "'";
			$result_batch = $mysqli->query($query_batch);
			
			if($result_batch->num_rows > 0) {
				// Get batch
				if($_GET['id'] == 'all' || !isset($_GET['id'])) {
					if($_GET['stocked'] == 'yes') {
						$query_batch = "SELECT * FROM batches WHERE(stock=1 AND user='" . checkUser() . "')";
						$result_batch = $mysqli->query($query_batch);
				
						$batches = array();
						while($row_batch = $result_batch->fetch_assoc()) {
							$batches[] = $row_batch['id'];
						}
					} elseif($_GET['stocked'] == 'no') {
						$query_batch = "SELECT * FROM batches WHERE(stock=0 AND user='" . checkUser() . "')";
						$result_batch = $mysqli->query($query_batch);
				
						while($row_batch = $result_batch->fetch_assoc()) {
							$batches[] = $row_batch['id'];
						}
					} else {
						$query_batch = "SELECT * FROM batches WHERE(stock=1 AND user='" . checkUser() . "')";
						$result_batch = $mysqli->query($query_batch);
				
						$batches = array();
						while($row_batch = $result_batch->fetch_assoc()) {
							$batches[] = $row_batch['id'];
						}
				
						$query_batch = "SELECT * FROM batches WHERE(stock=0 AND user='" . checkUser() . "')";
						$result_batch = $mysqli->query($query_batch);
				
						while($row_batch = $result_batch->fetch_assoc()) {
							$batches[] = $row_batch['id'];
						}
					}
				} else {
					$batches = explode(",", $_GET['id']);
				}
			
				foreach($batches as $btch) {
					$query_batch = "SELECT * FROM batches WHERE id='" . mysqli_real_escape_string($mysqli, $btch) . "'";
					$result_batch = $mysqli->query($query_batch);
			
					if($row_batch = $result_batch->fetch_assoc()) {
						$batch->data['id'] = $row_batch['id'];
						getBatch($batch);
				?>
				<div class="infobox" id="<?php echo $batch->data['id']; ?>">
					<div class="infobox_top">
						<span class="infobox_id"><?php echo $batch->data['id']; ?></span>
						<span class="infobox_sub"><?php echo getBatchSubstance($batch->data['id']); ?></span>
						<a href="./batch?id=<?php echo $batch->data['id']; ?>"><span class="infobox_batch"><?php echo $batch->data['id']; ?></span></a>
						<a href="./substance?id=<?php echo getBatchSubstanceID($batch->data['id']); ?>"><span class="infobox_subst"><?php echo getBatchSubstanceID($batch->data['id']); ?></span></a>
					</div>
				
					<?php if($batch->data['stock'] == 1) { ?>
						<div class="stock" id="stocked">Stocked</div>
					<?php } else { ?>
						<div class="stock" id="unstocked">Exhausted</div>
			        <?php } ?>
			
					<div class="infobox_content">				
						<table class="infobox_detail">
		                    <tr>
		                        <td class="infobox_detail_title" valign="top">Quantity</td>
		                        <td class="infobox_detail_content"><?php echo $batch->data['quantity']; ?></td>
		                    </tr>
		                    <?php if(strlen($batch->data['unitquantity']) > 0) { ?>
		                    <tr>
								<td class="infobox_detail_title" valign="top">Unit</td>
								<td class="infobox_detail_content"><?php echo $batch->data['unitquantity']; ?></td>
							</tr>
							<?php } ?>
							<tr>
								<td class="infobox_detail_title" valign="top">Form</td>
								<td class="infobox_detail_content"><?php echo $batch->data['form']; ?></td>
							</tr>
							<?php if(strlen($batch->data['vendor']) > 0 && $_GET['vendor'] == 'show') { ?>
							<tr>
								<td class="infobox_detail_title" valign="top">Vendor</td>
								<td class="infobox_detail_content"><?php echo $batch->data['vendor']; ?></td>
							</tr>
							<?php } ?>
						</table>

						<p class="infobox_notes"><?php echo $batch->data['description']; ?></p>
					
						<?php
							$directory = './assets/img/batches/' . $batch->data['id'] . '/';
					
							if(file_exists($directory)) {
								$files = scandir('./assets/img/batches/' . $batch->data['id']);
							
								foreach($files as $file) {
									$extension = pathinfo($file, PATHINFO_EXTENSION);
									$allowed = array('jpg', 'png');
									$checkfile = explode('.', $file);
							
									// Check if file is an image, and avoid listing thumbnails
									if(in_array($extension,$allowed) && substr($checkfile[0], -1) != 't') {
										// Lookup thumbnail
										$thumb = substr($file, 0, -4) . 't.jpg';
						?>
						<div class="infobox_pics">
							<a href="<?php echo $directory . $file; ?>" target="_blank">
								<img src="<?php echo $directory . $thumb; ?>" class="batchpic">
							</a>
						</div>
						<?php
									}
								}
							}
						?>
					</div>
				</div>
				<?php
					} else {
				?>
				<div class="infobox">
					<div class="infobox_content">
						No batch with ID '<?php echo $_GET['id']; ?>'.
					</div>
				</div>
			<?php
					}
				}
			} else {
		?>
				<div class="infobox">
					<div class="infobox_content">
						User has no batches or doesn't exist.
					</div>
				</div>
		<? } ?>
<?php include('footer.php'); ?>
