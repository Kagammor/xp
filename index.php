<?php include('header.php'); ?>
<div id="nav">
	sort by: 
	
	<?php
		$sortoptions = array("date down", "date up", "ID", "rating down", "rating up");
		
		foreach($sortoptions as $option) {
			$option_display = str_replace("down", "&#x25be;", $option);
			$option_display = str_replace("up", "&#x25b4;", $option_display);
			
			if($option == $prefsort) {
				echo "<span class=\"sortitem notsort\">" . $option_display . "</span>";
			} else {
				echo "<a class=\"sortitem\" href=\"?sort=" . $option . "\">" . $option_display . "</a>";
			}
		}
	?>
	
	<div id="links">
		<a href="./list">substances &#8594;</a> <span style="color: #aaa;">|<span> 
		<a href="./userlist">users &#8594;</a>
	</div>
</div>

<h2>Index</h2>

<?php
	if(checkUser() != NULL) {
		$user = " WHERE user='" . checkUser() . "'";
	}

	// Get experiences
	$query_xp = "SELECT * FROM experiences" . $user . " ORDER BY " . mysqli_real_escape_string($mysqli, $sort);
	$result_xp = $mysqli->query($query_xp);
	
	if($result_xp->num_rows > 0) {
		while($row_xp = $result_xp->fetch_assoc()) {
			$xp->data['id'] = $row_xp['id'];
			$usr->data['id'] = $row_xp['user'];
			getExperience($xp);
			getUser($usr);
	?>
	<div class="infobox" id="<?php echo $xp->data['id']; ?>">
		<div class="infobox_top">
			<span class="infobox_id"><?php echo $xp->data['id']; ?></span>
			<span class="infobox_sub"><a href="./experience?id=<?php echo $xp->data['id']; ?>"><?php echo $xp->data['substance']; ?></a></span>
			<a href="./batch?id=<?php echo $xp->data['batch']; ?>"><span class="infobox_batch"><?php echo $xp->data['batch']; ?></span></a>
			<a href="./substance?id=<?php echo getBatchSubstanceID($xp->data['batch']); ?>"><span class="infobox_subst"><?php echo getBatchSubstanceID($xp->data['batch']); ?></span></a>
		</div>
	
		<div class="infobox_content">
			<table class="infobox_detail">
				<tr>
					<td class="infobox_detail_title" valign="top">User</td>
					<td class="infobox_detail_content"><?php echo $usr->data['username']; ?></td>
				</tr>
				<tr>
					<td class="infobox_detail_title" valign="top">Rating</td>
					<td class="infobox_detail_content"><?php echo $xp->data['stars']; ?></td>
				</tr>
				<tr>
					<td class="infobox_detail_title" valign="top">Date</td>
					<td class="infobox_detail_content"><?php echo $xp->data['date']; ?></td>
				</tr>
				<tr>
					<td class="infobox_detail_title" valign="top">ROA</td>
					<td class="infobox_detail_content"><?php echo $xp->data['roa']; ?></td>
				</tr>
				<tr>
					<td class="infobox_detail_title" valign="top">Dosing</td>
					<td class="infobox_detail_content"><?php echo $xp->data['dosing']; ?></td>
				</tr>
				<tr>
					<td class="infobox_detail_title" valign="top">Setting</td>
					<td class="infobox_detail_content"><?php echo $xp->data['setting']; ?></td>
				</tr>
			</table>

			<p class="infobox_notes"><?php echo $xp->data['notes']; ?></p>
		</div>
	</div>
<?php
		}
	} else {
?>
	<div class="infobox">
		<div class="infobox_content">
			User has no experience logs or doesn't exist.
		</div>
	</div>
<?php
	}
	include('footer.php');
?>
