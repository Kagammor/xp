<?php
	// SORTING
	if(isset($_GET['sort'])) {
		$prefsort = $_GET['sort'];
	} else {
		$prefsort = "date down";
	}
	
	if($prefsort == 'date down') {
		$sort = 'date DESC';
	} elseif($prefsort == 'date up') {
		$sort = 'date';
	} elseif($prefsort == 'ID') {
		$sort = 'id';
	} elseif($prefsort == 'rating down') {
		$sort = 'rating DESC';
	} elseif($prefsort == 'rating up') {
		$sort = 'rating';
	} else {
		// Default option
		$sort = 'date DESC';
	}
?>
