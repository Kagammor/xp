<?php
	function getBatchSubstance($batch_id) {
		$mysqli = new mysqli('localhost','tripman','trip#man','trips');

		// Check if MySQL connection can be made
		if (mysqli_connect_errno())
		{
			echo "The connection to the MySQL database failed:<br />" . mysqli_connect_error();
			exit();
		}
	
		$query_batch = "SELECT * FROM batches WHERE id='" . $batch_id . "'";
		$result_batch = $mysqli->query($query_batch);
		$row_batch = $result_batch->fetch_assoc();
	
		$query_sub = "SELECT * FROM substances WHERE id='" . $row_batch['substance'] . "'";
		$result_sub = $mysqli->query($query_sub);
		$row_sub = $result_sub->fetch_assoc();

		return ucfirst($row_sub['substance']);
	}
	
	function getBatchSubstanceID($batch_id) {
		$mysqli = new mysqli('localhost','tripman','trip#man','trips');

		// Check if MySQL connection can be made
		if (mysqli_connect_errno())
		{
			echo "The connection to the MySQL database failed:<br />" . mysqli_connect_error();
			exit();
		}
		
		$ids = explode(',', $batch_id);
		
		$count = 0;
		foreach($ids as $id) {
			$query_batch = "SELECT * FROM batches WHERE id='" . $id . "'";
			$result_batch = $mysqli->query($query_batch);
			$row_batch = $result_batch->fetch_assoc();

			$query_sub = "SELECT * FROM substances WHERE id='" . $row_batch['substance'] . "'";
			$result_sub = $mysqli->query($query_sub);
			$row_sub = $result_sub->fetch_assoc();
			
			if($count > 0) {
				echo ',' . $row_sub['id'];
			} else {
				echo $row_sub['id'];
			}
			
			$count++;
		}
	}
	
	Class getBA {
		public $batch = array();
	}
	
	$batch = new getBA;
	
	function getBatch(getBA $obj) {
		$mysqli = new mysqli('localhost','tripman','trip#man','trips');

		// Check if MySQL connection can be made
		if (mysqli_connect_errno())
		{
			echo "The connection to the MySQL database failed:<br />" . mysqli_connect_error();
			exit();
		}
		
		$query_batch = "SELECT * FROM batches WHERE ID='" . $obj->data['id'] . "'";
		$result_batch = $mysqli->query($query_batch);
		$row_batch = $result_batch->fetch_assoc();
		
		if($row_batch['unit'] == "tabs") {
			$obj->data['unit'] = " " . $row_batch['unit'];
		} else {
			$obj->data['unit'] = $row_batch['unit'];
		}
		
		$obj->data['unitquantity'] = $row_batch['unitquantity'];
		
		$obj->data['quantity'] = $row_batch['quantity'];
		$obj->data['form'] = ucfirst($row_batch['form']);
		$obj->data['vendor'] = $row_batch['vendor'];
		$obj->data['description'] = $row_batch['description'];
		$obj->data['stock'] = $row_batch['stock'];
	}
?>
