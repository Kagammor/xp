<?php
	Class getXP {
		public $xp = array();
	}
	
	$xp = new getXP;
	
	function getExperience(getXP $obj) {
		$mysqli = new mysqli('localhost','tripman','trip#man','trips');

		// Check if MySQL connection can be made
		if (mysqli_connect_errno())
		{
			echo "The connection to the MySQL database failed:<br />" . mysqli_connect_error();
			exit();
		}
		
		$query_xp = "SELECT * FROM experiences WHERE ID='" . $obj->data['id'] . "'";
		$result_xp = $mysqli->query($query_xp);
		$row_xp = $result_xp->fetch_assoc();
		
		// Parse date to exclude day when unspecified
		$date_frac = explode('-', $row_xp['date']);
		if($date_frac[2] == '00 00:00:00') {
			$date = strtotime($row_xp['date']);
			$obj->data['date'] = date('F Y', $date);
		} else {
			$date = strtotime($row_xp['date']);
			$obj->data['date'] = date('l, F j, Y, H:i:s', $date);
		}

		$batch_id = explode(',', $row_xp['batch']);
		$count = 0;
		
		// SUBSTANCES
		// Handle multiple substances
		while($count < count($batch_id)) {
			// Format correctly based on whether or not entry contains multiple substance
			if($count < 1) {
				$obj->data['substance'] = ucfirst(getBatchSubstance($batch_id[$count]));
			} else {
				$obj->data['substance'] = $obj->data['substance'] . ', ' . ucfirst(getBatchSubstance($batch_id[$count]));
			}
			
			$count++;
		}
		
		// RATING
		$count = 0;
		$stars = '';
		while($count < $row_xp['rating']) {
			$stars = $stars . '&#9733;';
			$count++;
		}
		while($count < 10) {
			$stars = $stars . '&#9734;';
			$count++;
		}
		
		$obj->data['stars'] = $stars;
		
		// ROA
		if(strpos($row_xp['roa'], '.')) {
			$roa = explode(".", $row_xp['roa']);

			$count = 0;
			$admin = '';
			foreach($roa as $route) {
				$substance = getBatchSubstance($batch_id[$count]);
			
				$substance = ucfirst($substance);

                $admin = $admin . "<span class=\"exp_detail_dose\">" . $substance . "</span>: " . ucfirst($route) . "<br>";
				
				$count++;
			}
			
			$obj->data['roa'] = $admin;
		} else {
			$obj->data['roa'] = ucfirst($row_xp['roa']);
		}
		
		// DOSING
		$dosing = str_replace(",", "\n", $row_xp['dosing']);
		$dosing = str_replace("!", " to ", $dosing);
		
		if(strpos($row_xp['dosing'], '|')) {
			$dosing = explode('|', $dosing);

			$count = 0;
			$dosage = '';
			foreach($dosing as $dose) {
				$substance = getBatchSubstance($batch_id[$count]);
				
				$dosage = $dosage . "<span class=\"exp_detail_dose\">" . $substance . "</span><br><span>" . $dose . "</span><br>";
				
				$count++;
			}
			
			$obj->data['dosing'] = nl2br($dosage);
		} else {
			$obj->data['dosing'] = nl2br($dosing);
		}
		
		
		// ID, BATCH, SETTING, NOTES
		$obj->data['id'] = $row_xp['id'];
		$obj->data['batch'] = $row_xp['batch'];
		$obj->data['setting'] = $row_xp['setting'];
		$obj->data['notes'] = $row_xp['notes'];	
	}
?>
