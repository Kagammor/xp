<?php
	session_start();
	
	include('db.php');
	
	// Login
	if(isset($_POST['login'])) {
		$query_user = "SELECT * FROM users WHERE username='" . mysqli_real_escape_string($mysqli, $_POST['username']) . "'";
		$result_user = $mysqli->query($query_user);
		$row_user = $result_user->fetch_assoc();
		
		$hash    = hash('sha256', hash('md5', $_POST['password']));
		
		if($result_user->num_rows < 1) {
			header('Location: ../login.php?fb=' . base64_encode("User doesn't exist!"));
		} elseif($hash != $row_user['password']) {
			header('Location: ../login.php?fb=' . base64_encode("Wrong password!"));
		} else {
			$_SESSION['auth'] = $row_user['id'];
			header('Location: ../');
		}
	}
	
	// Logout
	if(isset($_GET['logout'])) {
		unset($_SESSION['auth']);
		session_destroy();
		
		header('Location: ../login?fb=' . base64_encode('Logged out.'));
	}
	
	// Register
	if(isset($_POST['register'])) {
		$query_user = "SELECT * FROM users WHERE username='" . mysqli_real_escape_string($mysqli, $_POST['username']) . "'";
		$result_user = $mysqli->query($query_user);
		
		// Check data
		if(strlen($_POST['username']) < 3) {
			header('Location: ../register.php?fb=' . base64_encode('Username too short!'));
		} elseif($result_user->num_rows > 0) {
			header('Location: ../register.php?fb=' . base64_encode('Username exists!'));
		} elseif(strlen($_POST['password']) < 4) {
			header('Location: ../register.php?fb=' . base64_encode('Password too short!'));
		} else {
			// Insert authentication details
			$hash = hash('sha256', hash('md5', $_POST['password']));

			$query_auth = "INSERT INTO users (username, password) VALUES ('" . mysqli_real_escape_string($mysqli, $_POST['username']) . "', '" . mysqli_real_escape_string($mysqli, $hash) . "')";
			$result_auth = $mysqli->query($query_auth);
			
			if(!$result_auth) {
				header('Location: ../register.php?fb=' . base64_encode('Database error.'));
				exit();
			} else {
				header('Location: ../login?fb=' . base64_encode('Registration successful!') . '&c=g');
			}
		}
	}
?>
