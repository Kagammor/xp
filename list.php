<?php include('header.php'); ?>
		<div id="nav">
			<a href="./">&#8592; Index</a>
		</div>
		
		<h2>List</h2>
		
		<form method="get" action="substance">
		<?php
			$query_cat = "SELECT * FROM categories";
			$result_cat = $mysqli->query($query_cat);
			
			while($row_cat = $result_cat->fetch_assoc()) {
		?>
			<div class="infobox">
				<div class="infobox_top">
					<span class="infobox_sub"><?php echo ucfirst($row_cat['category']); ?></span>
				</div>
		
				<div class="infobox_content">
					<table class="list">
						<tr class="list_head">
							<td class="list_head_select"></td>
							<td class="list_head_sub">Substance</td>
							<td class="list_head_summary">Summary</td>
						</tr>
					<?php
						$query_sub = "SELECT * FROM substances WHERE category='" . $row_cat['id'] . "' ORDER BY substance";
						$result_sub = $mysqli->query($query_sub);
			
						while($row_sub = $result_sub->fetch_assoc()) {
					?>
						<tr>
							<td><input type="checkbox" name="id" value="<?php echo $row_sub['id']; ?>"></td>
							<td class="list_substance"><a href="./substance?id=<?php echo $row_sub['id']; ?>"><?php echo ucfirst($row_sub['substance']); ?></a></td>
							<td class="list_summary">
							<?php
								$sub->data['id'] = $row_sub['id'];
								getSubstance($sub);
							
								echo $sub->data['summary'];
							?>
							</td>
						</tr>
				<?php
						}
				?>
					</table>
				</div>
			</div>
		<?php
			}
		?>
			<div class="list_item_buttons">
				<input type="submit" class="button" value="Show selected">
				<a href="./substance?id=all"><input type="submit" class="button" value="Show all"></a>
			</div>
		</form>
<?php include('footer.php'); ?>
