<?php
	session_start();

	require_once('./proc/db.php');
	require_once('./proc/sort.php');
	require_once('./proc/batches.php');
	require_once('./proc/exps.php');
	require_once('./proc/subs.php');
	require_once('./proc/user.php');
?>
<!doctype html>
<html>
	<head>
		<title>Triplog</title>
	
		<link href="./assets/css/style.css" type="text/css" rel="stylesheet">
	</head>
	
	<body>
		<header>
			<h1>Triplog</h1>

			<div class="userpanel">
			<?php
				if(isset($_SESSION['auth'])) {
					$usr->data['id'] = $_SESSION['auth'];
					getUser($usr);
			?>
				<div class="userpanel_welcome">Welcome, <?php echo $usr->data['username']; ?>!</div>
				<div class="userpanel_profile"><a href="./profile">Profile</a></div>
				<div class="userpanel_logout"><a href="./proc/auth?logout">Logout</a></div>
			<?php } else { ?>
				<div class="userpanel_login"><a href="login">Log in</a></div>
			<?php } ?>
			</div>
		</header>
