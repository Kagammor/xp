<?php include('header.php'); ?>
<div id="nav">
	<a href="./">&#8592; Index</a>
</div>

<h2>Experience</h2>

		
<div class="infobox" id="<?php echo $xp->data['id']; ?>">
<?php
	// Get experience
	$experiences = explode(",", $_GET['id']);
	
	foreach($experiences as $experience) {
		$query_xp = "SELECT * FROM experiences WHERE id='" . mysqli_real_escape_string($mysqli, $experience) . "'";
		$result_xp = $mysqli->query($query_xp);
	
		if($row_xp = $result_xp->fetch_assoc()) {
			$xp->data['id'] = $row_xp['id'];
			getExperience($xp);
	?>
		<div class="infobox_top">
			<span class="infobox_id"><?php echo $xp->data['id']; ?></span>
			<span class="infobox_sub"><?php echo $xp->data['substance']; ?></span>
			<a href="./batch?id=<?php echo $xp->data['batch']; ?>"><span class="infobox_batch"><?php echo $xp->data['batch']; ?></span></a>
			<a href="./substance?id=<?php echo getBatchSubstanceID($xp->data['batch']); ?>"><span class="infobox_subst"><?php echo getBatchSubstanceID($xp->data['batch']); ?></span></a>
		</div>
	
		<div class="infobox_content">
			<table class="infobox_detail">
				<tr>
					<td class="infobox_detail_title" valign="top">Rating</td>
					<td class="infobox_detail_content"><?php echo $xp->data['stars']; ?></td>
				</tr>
				<tr>
					<td class="infobox_detail_title" valign="top">Date</td>
					<td class="infobox_detail_content"><?php echo $xp->data['date']; ?></td>
				</tr>
				<tr>
					<td class="infobox_detail_title" valign="top">ROA</td>
					<td class="infobox_detail_content"><?php echo $xp->data['roa']; ?></td>
				</tr>
				<tr>
					<td class="infobox_detail_title" valign="top">Dosing</td>
					<td class="infobox_detail_content"><?php echo $xp->data['dosing']; ?></td>
				</tr>
				<tr>
					<td class="infobox_detail_title" valign="top">Setting</td>
					<td class="infobox_detail_content"><?php echo $xp->data['setting']; ?></td>
				</tr>
			</table>

			<p class="infobox_notes"><?php echo $xp->data['notes']; ?></p>
		</div>
	<?php } else { ?>
		<div class="infobox_content">
			No experience with ID '<?php echo $experience; ?>'.
		</div>
<?php
		}
	}
?>
</div>
<?php include('footer.php'); ?>
