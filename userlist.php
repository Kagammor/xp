<?php include('header.php'); ?>
<div id="nav">
	<a href="./">&#8592; Index</a>
</div>

<h2>Users</h2>
<?php
	// Get users
	$query_users = "SELECT * FROM users";
	$result_users = $mysqli->query($query_users);
	
	if($result_users->num_rows > 0) {
?>
<div class="infobox">
	<div class="infobox_content">
		<table class="list">
			<tr class="list_head">
				<td class="list_head_id">ID</td>
				<td class="list_head_username">Username</td>
				<td class="list_head_count">Experiences</td>
				<td class="list_head_count">Batches</td>
				<td>Bio</td>
			</tr>
<?php
		while($row_users = $result_users->fetch_assoc()) {
?>
			<tr>
				<td><?php echo $row_users['id']; ?></td>
				<td><?php echo $row_users['username']; ?></td>
				<td><a href="./index?user=<?php echo $row_users['id']; ?>"><?php echo getUserExperiences($row_users['id']); ?></a></td>
				<td><a href="./batch?user=<?php echo $row_users['id']; ?>"><?php echo getUserBatches($row_users['id']); ?></a></td>
				<td><?php echo $row_users['bio']; ?></td>
			</tr>
<?php } ?>
		</table>
	</div>
</div>
<?php } else { ?>
<div class="infobox">
	<div class="infobox_content">
		There are no registered users.
	</div>
</div>
<?php
	}
	include('footer.php');
?>
