<?php include('header.php'); ?>
<div id="nav">
	<a href="./">&#8592; Index</a>
</div>

<h2>Profile</h2>

<div class="infobox">
	<div class="infobox_content">
		<?php if(isset($_GET['fb'])) { ?>
		<div class="form_feedback<?php if($_GET['c'] == 'g') { echo ' form_feedback_green'; }; ?>"><?php echo base64_decode($_GET['fb']); ?></div>
		<?php } ?>
	
		<form method="post" action="./proc/update.php" class="profile">
			<ul>
				<?php
					$usr->data['id'] = $_SESSION['auth'];
					getUser($usr);
				?>
				<li>
					<h3>Username</h3>
					<input type="text" name="username" class="form_username" value="<?php echo $usr->data['username']; ?>">
				</li>
				<li>
					<h3>Password</h3>
					<ul>
						<li><input type="password" name="password_old" class="form_password" placeholder="Old password"></li>
						<li><input type="password" name="password_new_1" class="form_password" placeholder="New password"></li>
						<li><input type="password" name="password_new_2" class="form_password" placeholder="Repeat new password"></li>
					</ul>
				</li>
				<li>
					<h3>Bio</h3>
					<textarea class="profile_bio"><?php echo $usr->data['bio']; ?></textarea>
				</li>
			</ul>
		</form>
	</div>
</div>

</form>
<?php include('footer.php'); ?>
