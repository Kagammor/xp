<?php include('header.php'); ?>
<div id="nav">
	<a href="./">&#8592; Index</a>
</div>

<form id="form_auth" method="post" action="./proc/auth.php">
	<h2 style="text-align: center;">Register</h2>

	<?php if(isset($_GET['fb'])) { ?>
	<div class="form_feedback<?php if($_GET['c'] == 'g') { echo ' form_feedback_green'; }; ?>"><?php echo base64_decode($_GET['fb']); ?></div>
	<?php } ?>

	<ul>
		<li><input type="text" name="username" placeholder="Username" class="form_username"></li>
		<li><input type="password" name="password" placeholder="Password" class="form_password"></li>
		<li><input type="submit" name="register" value="Register"></li>
	</ul>
	
	<div class="form_sidenote">Have an account? <a href="./login">Login!</a></div>
</form>
<?php include('footer.php'); ?>
