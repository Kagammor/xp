<?php include('header.php'); ?>
<div id="nav">
	<a href="./">&#8592; Index</a> <span style="color: #aaa;">|<span> 
	<a href="./list">&#8592; list</a>
</div>

<h2>Substance</h2>

<?php
	// Get substance
	if(strpos($_SERVER['REQUEST_URI'], "&id=")) {
		$substances = explode("?id=", $_SERVER['REQUEST_URI']);
		$substances = explode("&id=", $substances[1]);
	} elseif($_GET['id'] == 'all' || !isset($_GET['id'])) {
		$query_sub = "SELECT * FROM substances";
		$result_sub = $mysqli->query($query_sub);
		
		$substances = array();
		while($row_sub = $result_sub->fetch_assoc()) {
			$substances[] = $row_sub['id'];
		}
	} else {
		$substances = explode(",", $_GET['id']);
	}
	
	foreach($substances as $sub) {
		$query_sub = "SELECT * FROM substances WHERE id='" . mysqli_real_escape_string($mysqli, $sub) . "'";
		$result_sub = $mysqli->query($query_sub);
	
		if($row_sub = $result_sub->fetch_assoc()) {
			$substance = ucfirst($row_sub['substance']);
			
			$json = file_get_contents("http://tripbot.tripsit.me/api/tripsit/getDrug?name=" . $row_sub['substance']);
			$data = json_decode($json, TRUE);
?>
<div class="infobox" id="<?php echo $sub->data['id']; ?>">
	<div class="infobox_top">
		<span class="infobox_id"><?php echo $row_sub['id']; ?></span>
		<span class="infobox_sub"><?php echo $substance; ?></span>
	</div>
	
	<div class="infobox_content">
		<?php					
			if($data['data'][0]) {
				$aliases = $data['data'][0]['properties']['aliases'];
				$aka = '';
				$count = 0;
				foreach($aliases as $alias) {
					if($count > 0) {
						$aka = $aka . ', ' . $alias;
					} else {
						$aka = $alias;
					}
		
					$count++;
				}
				$aliases = ucfirst($aka);
	
				$dose = $data['data'][0]['properties']['dose'];
				$dose = str_replace(" | ", "<br><br>", $dose);
				if(substr($dose, 0, 9) != 'Threshold') {
					$dose = str_ireplace("Threshold", "<br><span style=\"color: #a5a;\">Threshold</span>", $dose);
				} else {
					$dose = str_ireplace("Threshold", "<span style=\"color: #a5a;\">Threshold</span>", $dose);
				}
				if(substr($dose, 0, 5) != 'Light') {
					$dose = str_ireplace("Light", "<br><span style=\"color: #5a5;\">Light</span>", $dose);
				} else {
					$dose = str_ireplace("Light", "<span style=\"color: #5a5;\">Light</span>", $dose);
				}
				$dose = str_ireplace("Common", "<br><span style=\"color: #5af;\">Common</span>", $dose);
				$dose = str_ireplace("Strong", "<br><span style=\"color: #c80;\">Strong</span>", $dose);
				$dose = str_ireplace("Heavy", "<br><span style=\"color: #f00;\">Heavy</span>", $dose);
	
				$duration = $data['data'][0]['properties']['duration'];
	
				$onset = $data['data'][0]['properties']['onset'];
				$onset = str_replace(" | ", "<br>", $onset);
	
				$effects = ucfirst($data['data'][0]['properties']['effects']);
				$aftereffects = ucfirst($data['data'][0]['properties']['after-effects']);
				$tolerance = $data['data'][0]['properties']['tolerance'];
				$avoid = ucfirst($data['data'][0]['properties']['avoid']);
				$marquis = $data['data'][0]['properties']['marquis'];
				$summary = $data['data'][0]['properties']['summary'];
		?>
		<p class="infobox_notes"><?php echo $summary; ?></p>

		<table class="infobox_detail">
			<?php if(strlen($aliases) > 0) { ?>
			<tr>
				<td class="infobox_detail_title" valign="top">Aliases</td>
				<td class="infobox_detail_content"><?php echo $aliases; ?></td>
			</tr>
			<?php 
				}
				if(strlen($effects) > 0) {
			?>
			<tr>
				<td class="infobox_detail_title" valign="top">Effects</td>
				<td class="infobox_detail_content"><?php echo $effects; ?></td>
			</tr>
			<?php 
				}
				if(strlen($dose) > 0) {
			?>
			<tr>
				<td class="infobox_detail_title" valign="top">Dose</td>
				<td class="infobox_detail_content"><?php echo $dose; ?></td>
			<?php 
				}
				if(strlen($duration) > 0) {
			?>
			<tr>
				<td class="infobox_detail_title" valign="top">Duration</td>
				<td class="infobox_detail_content"><?php echo $duration; ?></td>
			</tr>
			<?php 
				}
				if(strlen($onset) > 0) {
			?>
			<tr>
				<td class="infobox_detail_title" valign="top">Onset</td>
				<td class="infobox_detail_content"><?php echo $onset; ?></td>
			</tr>
			<?php 
				}
				if(strlen($aftereffects) > 0) {
			?>
			<tr>
				<td class="infobox_detail_title" valign="top">After-effects</td>
				<td class="infobox_detail_content"><?php echo $aftereffects; ?></td>
			</tr>
			<?php 
				}
				if(strlen($tolerance) > 0) {
			?>
			<tr>
				<td class="infobox_detail_title" valign="top">Tolerance</td>
				<td class="infobox_detail_content"><?php echo $tolerance; ?></td>
			</tr>
			<?php 
				}
				if(strlen($avoid) > 0) {
			?>
			<tr>
				<td class="infobox_detail_title" valign="top">Avoid</td>
				<td class="infobox_detail_content"><?php echo $avoid; ?></td>
			</tr>
			<?php 
				}
				if(strlen($marquis) > 0) {
			?>
			<tr>
				<td class="infobox_detail_title" valign="top"><a href="http://en.wikipedia.org/wiki/Marquis_reagent" target="_blank">Marquis</a></td>
				<td class="infobox_detail_content"><?php echo $marquis; ?></td>
			</tr>
			<?php } ?>
		</table>
		<?php } else { ?>
		No info about <span style="font-style: italic;"><?php echo $substance; ?></span>.
	<?php
			}
		} else {
	?>
		<div class="infobox_content">
			No substance with ID '<?php echo $_GET['id']; ?>'.
		</div>
	<?php } ?>
	</div>
</div>
<?php } ?>
	
<span id="tripsit">Data provided by <a href="http://wiki.tripsit.me/wiki/Factsheets" target="_blank"><img src="./assets/img/tripsit.png" alt="TripSit"></a></span>
<?php include('footer.php'); ?>
